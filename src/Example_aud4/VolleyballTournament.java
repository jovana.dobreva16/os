
package Examples;

import java.util.HashSet;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class VolleyballTournament {


    public static void main(String[] args) throws InterruptedException {
        HashSet<Player> threads = new HashSet();

        for (int i = 0; i < 60; ++i) {
           Player player = new Player();
            threads.add(player);
        }

        // run all threads in background
        for(Player player : threads)
           player.start();
        // after all of them are started, wait each of them to finish for maximum 2_000 ms
        for(Player player : threads)
           player.join(2000);

        // for each thread, terminate it if it is not finished
       for(Player player : threads){
           if(player.isAlive()) {
               System.out.println("Possible deadlock!");
               player.interrupt();
           }
       }
        System.out.println("Tournament finished.");

    }
}




class Player extends Thread {
    static Semaphore sala = new Semaphore(12);
    static Semaphore soblekuvalna = new Semaphore(4);
    static Incrementor soblekuvalnaIgraci = new Incrementor();
    static Incrementor igraciVoSala = new Incrementor();
    static Incrementor igraciKoiGoZavrshileNatprevarot = new Incrementor();
    static Semaphore natprevar = new Semaphore(1);
   

    Player() {
    }

    public void execute() throws InterruptedException {
        this.sala.acquire();
        System.out.println("Player inside.");
        this.soblekuvalna.acquire();
        soblekuvalnaIgraci.increment();

        System.out.println("In dressing room.");

        if (soblekuvalnaIgraci.getCounter() == 4) {
            soblekuvalnaIgraci.setCounter(0);
            this.soblekuvalna.release(4);
        }


        Thread.sleep(10);

        igraciVoSala.increment();

        if (igraciVoSala.getCounter() == 12) {

            igraciVoSala.setCounter(0);
            this.natprevar.acquire();
            System.out.println("Game started.");
        }



        Thread.sleep(100);
        System.out.println("Player done.");
        igraciKoiGoZavrshileNatprevarot.increment();

        if (igraciKoiGoZavrshileNatprevarot.getCounter() == 12) {
            System.out.println("Game finished.");
            igraciKoiGoZavrshileNatprevarot.setCounter(0);
            this.sala.release(12);

            this.natprevar.release();

        }

    }

    public void run() {
        try {
            this.execute();
        } catch (InterruptedException var2) {
            var2.printStackTrace();
        }

    }
}



class Incrementor {
      int  counter = 0;
    Lock lock = new ReentrantLock();

    Incrementor() {
    }

    public void increment() {
        this.lock.lock();
        counter++;
        this.lock.unlock();
    }

    public void setCounter(int counter) {
        this.lock.lock();
        this.counter = counter;
        this.lock.unlock();
    }

    public synchronized int getCounter() {
        return this.counter;
    }
}
