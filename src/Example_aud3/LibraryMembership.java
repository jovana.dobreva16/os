package Examples;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class LibraryMembership {
    public static Semaphore rowOfMembers = new Semaphore(10);
    public static Semaphore passTheDoc = new Semaphore(1);
    public  static Semaphore storeTheDoc = new Semaphore(1);
    public  static Semaphore done = new Semaphore(1);

    static class Member extends Thread{
        String name;

        public Member(String name){
            this.name = name;
        }

        public void passTheDocuments() throws InterruptedException {
            rowOfMembers.acquire();//1 2 3 4 5 6 7 8 9 10
            passTheDoc.acquire();
            System.out.println("Member "+name+"passed the documents");
            storeTheDoc.release();
            done.acquire();

        }

        @Override
        public void run() {

            for(int i = 0; i<10; i++){
                try {
                    passTheDocuments();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static  class LibraryWorker extends Thread{
        String name;
        SharedSpace space;



        public LibraryWorker(String name, SharedSpace space){
            this.name = name;
            this.space = space;
        }

        public void storeTheDocuments() throws InterruptedException {
            storeTheDoc.acquire();
            System.out.println("The worker: "+name +"stored the documents");
            passTheDoc.release();
            done.release();
            rowOfMembers.release();
            space.increment();

        }

        @Override
        public void run() {
            for(int i = 0; i<10; i++){
                try {
                    storeTheDocuments();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        List<Member> members = new ArrayList<>();
        List<LibraryWorker> workers = new ArrayList<>();
        SharedSpace space = new SharedSpace();

        for (int i = 0; i<5; i++){
            members.add(new Member("M"+i));
            workers.add(new LibraryWorker("W"+i,space));


        }
        for(Member member: members)
            member.start();
        for(LibraryWorker worker: workers)
            worker.start();

        for(Member member: members)
            member.join();
        for(LibraryWorker worker: workers)
            worker.join();

        System.out.println("Number of new members: "+space.getCounter());
    }

    static class SharedSpace{
        static int counter = 0;
         void increment(){
            counter++;
        }

        int getCounter(){
            return counter;
        }
    }


}

