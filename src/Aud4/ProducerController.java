package Aud4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class ProducerController {

    static Semaphore accessBuffer;
    static Semaphore canCheck;
    static int numChecks = 0;
    static Semaphore lock;

    public static void init(){
        lock = new Semaphore(1);


        accessBuffer = new Semaphore(1);
        canCheck = new Semaphore(10);
    }

    static class Buffer{
        public void produce(){
            System.out.println("Producer is producing .........");
        }

        public void check(){
            System.out.println("Controller is checking .........");
        }
    }

    static class Producer extends Thread{
        private final Buffer buffer;


        public Producer(Buffer buffer) {
            this.buffer = buffer;
        }

        public void execute() throws InterruptedException {
            //request for Buffer access
            accessBuffer.acquire();
            //CD
            buffer.produce();
            accessBuffer.release();
        }

        @Override
        public void run() {
            try {
                execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class Controller extends Thread{
        private final Buffer buffer;

        Controller(Buffer buffer){
            this.buffer = buffer;
        }

        public void execute() throws InterruptedException {
            //check if there is more then 10 controllers ?
            lock.acquire();
            //CD numChecks
            if(numChecks ==0)
                accessBuffer.acquire();

            numChecks++;
            lock.release();

            canCheck.acquire();
            buffer.check();
            //finish
            lock.acquire();
            numChecks--;
            canCheck.release();
            if(numChecks == 0)
                accessBuffer.release();
            lock.release();
        }

        @Override
        public void run() {
            try {
                execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }



    public static void main(String[] args) throws InterruptedException {
        init();
        Buffer buffer =  new Buffer();
        List<Producer> producers = new ArrayList<>();
        List<Controller> controllerList = new ArrayList<>();
        for(int i = 0; i < 10; i++)
            controllerList.add(new Controller(buffer));
        for(int i = 0; i < 10; i++)
            producers.add(new Producer(buffer));
        for(Producer producer : producers)
            producer.start();
        for(Controller controller : controllerList)
            controller.start();
//
//        for(Producer producer : producers)
//            producer.join();
//       for(Controller controller : controllerList)
//           controller.join();
        System.out.println("End of the program");
    }
}




