import java.io.*;

public class Zadaca2 {

    public static void main(String[] args) throws IOException {
        BufferedReader bf = new BufferedReader(new FileReader("test.txt"));
    //1: reads and prints the 2nd line of the test.txt
        //1 solution
//        String tmp = bf.readLine();
//        tmp = bf.readLine();
//        System.out.println(tmp);
        int symbol = 0;
        int lineCounter = 0;
        while((symbol = bf.read()) != -1){
            if((char)symbol == '\n')
                lineCounter++;
            if(lineCounter==1 && (char)symbol!='\n')
                System.out.print((char)symbol);
        }

        //2: 2nd line rewrite it in reverse order

        RandomAccessFile raf = new RandomAccessFile(new File("test.txt"),"rw");
        raf.seek(20);

        String s = new StringBuilder(raf.readLine()).reverse().toString();
        System.out.println();

        raf.seek(20);
        raf.writeBytes(s);
        raf.close();
        bf.close();

    }
}
