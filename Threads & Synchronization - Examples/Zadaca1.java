import java.awt.print.Book;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Semaphore;

public class Zadaca1 {
    public static Semaphore semaphore1 = new Semaphore(0);
    public static Semaphore semaphore2 = new Semaphore(1);
    public static Semaphore semaphore3 = new Semaphore(2);

    public static void main(String[] args) throws InterruptedException {
        BookShelf bookShelf = new BookShelf();
        List<Thread> books = new LinkedList<>();
        for (int i = 0; i < 5; i++) {
            books.add(new BookB(bookShelf));
            books.add(new BookA(bookShelf));
        }
        for (Thread book : books) {
            book.start();
        }
        for(Thread book:books){
            book.join();
        }
        System.out.println(bookShelf.toString());
    }
}

class BookShelf{
    ArrayBlockingQueue<String> books = new ArrayBlockingQueue<>(100);
    public void addBook(String book){books.add(book);}
    public String toString(){
        String s = books.toString().substring(1,books.toString().length()-1).replace(", ","");
        return  s;
    }
}

class BookA extends Thread{
    BookShelf bookShelf;

    public BookA(BookShelf bookShelf) {
        this.bookShelf = bookShelf;
    }
    public void execute() throws InterruptedException {
        Zadaca1.semaphore2.acquire();
        bookShelf.addBook("A");
        Zadaca1.semaphore3.acquire();
        bookShelf.addBook("A");
        Zadaca1.semaphore3.release();
        Zadaca1.semaphore1.release();

    }
    @Override
    public void run() {
        try {
            execute();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class BookB extends Thread{
    BookShelf bookShelf;

    public BookB(BookShelf bookShelf) {
        this.bookShelf = bookShelf;
    }

    public void execute() throws InterruptedException {
        Zadaca1.semaphore1.acquire();
        bookShelf.addBook("B");
        Zadaca1.semaphore2.release();
    }

    @Override
    public void run() {
        try {
            execute();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}


