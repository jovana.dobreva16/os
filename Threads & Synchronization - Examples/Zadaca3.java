//public class Zadaca3 {
//    public static void main(String[] args) throws InterruptedException {
//        SharedResource resource = new SharedResource();
//        Thread t1 = new CustomThread(resource);
//        Thread t2 = new Thread(new CustomRunnable(resource));
//        t2.start();
//        Thread t3 = new CustomThread(resource);
//        t2.join();
//        t3.run();
//        t3.join();
//        t1.start();
//        resource.increment();
//        int finalCounter = resource.counter;
//        resource.increment();
//        System.out.println(finalCounter);
//
//    }
//}
//
//class SharedResource{
//    int counter;
//    public SharedResource(){counter=0;}
//    public void increment(){counter++;}
//}
//
//class CustomThread extends Thread{
//    SharedResource sharedResource;
//
//    public CustomThread(SharedResource sharedResource) {
//        this.sharedResource = sharedResource;
//
//    }
//
//    @Override
//    public void run() {
//        for(int i = 0;i<3;i++) {
//            this.sharedResource.increment();
//            System.out.print("T");
//        }
//    }
//}
//
//class CustomRunnable implements Runnable{
//    SharedResource sharedResource;
//
//    public CustomRunnable(SharedResource sharedResource) {
//        this.sharedResource = sharedResource;
//    }
//
//    @Override
//    public void run() {
//        for(int i = 0;i<3;i++) {
//            this.sharedResource.increment();
//            System.out.print("R");
//        }
//
//    }
//}
