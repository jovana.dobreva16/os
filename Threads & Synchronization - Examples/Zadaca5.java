import java.util.LinkedList;

public class Zadaca5 {
    public static void main(String[] args) throws InterruptedException {
        SharedResource sharedResource = new SharedResource();
        Producer producer = new Producer(sharedResource);
        Consumer consumer = new Consumer(sharedResource);

        producer.start();
        consumer.start();

        producer.join();
        consumer.join();
    }
}
class SharedResource{
    LinkedList<Integer> list = new LinkedList<>();
    int capacity = 15;

    //povikana koga ke dojde producer thread
    public void produce() throws InterruptedException {
        int value = 0;
        while(true){
            synchronized (this) {
                //dali magacinot e poln
                while (list.size() == capacity)
                    wait();
                System.out.println("Producer produced-"+value);
                list.add(value++);
                notify();
                Thread.sleep(1000);
            }
        }
    }

    public void consume() throws InterruptedException {
        while(true){

            synchronized (this){
                while(list.size()==0){
                    wait();
                }
                int val = list.removeFirst();
                System.out.println("Consumer consumed-"+val);

                notify();
                Thread.sleep(1000);
            }

        }
    }
}

class Producer extends Thread{
    SharedResource sharedResource;

    public Producer(SharedResource sharedResource) {
        this.sharedResource = sharedResource;
    }

    @Override
    public void run() {
        try {
            sharedResource.produce();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class Consumer extends Thread{
    SharedResource sharedResource;

    public Consumer(SharedResource sharedResource) {
        this.sharedResource = sharedResource;
    }

    @Override
    public void run() {
        try {
            sharedResource.consume();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}